#include "inter.h"
#include "wavebomb.h"
#include <math.h>
#include "planelist.h"
#include <stdio.h>
static double distan(jet jetaxe,tsar bulletaxe)
{
    return sqrt((jetaxe.x-bulletaxe.x)*(jetaxe.x-bulletaxe.x)+(jetaxe.y-bulletaxe.y)*(jetaxe.y-bulletaxe.y));
}
static jet truecenter(jet jetaxe)
{
    if(jetaxe.isenemy==1)
    {
        jetaxe.x+=10;
        jetaxe.y+=20;

    }
    else if(jetaxe.isenemy==0)
    {
        jetaxe.x+=20;
        jetaxe.y+=15;

    }
    return jetaxe;
}
static tsar *found(tsar target,tsar *myjet)
{
    tsar *scan,*temp;
    scan=myjet;
    temp=scan;
    int a=0;
    while(1)
    {
        if(scan->x==target.x&&scan->y==target.y)
        {

            return temp;
        }
        else
        {
            temp=scan;
            scan=scan->next;
        }
    }


}
void creattsarbomb(bullet axe,tsar **mytsar)
{
    tsar newtsar;
    newtsar.x=axe.x;
    newtsar.y=axe.y;
    newtsar.rad=0;
    newtsar.next=NULL;
    addtsar(newtsar,mytsar);
}
void creattsarbomb1(jet axe,tsar **mytsar)
{
    tsar newtsar;
    newtsar.x=axe.x;
    newtsar.y=axe.y;
    newtsar.rad=0;
    newtsar.next=NULL;
    addtsar(newtsar,mytsar);
}
void addtsar(tsar newtsar,tsar **mytsar)
{
    tsar *temp;
    temp=(tsar*)malloc(sizeof(tsar));

    *temp=newtsar;
    temp->next=NULL;

    tsar *scan=*mytsar;
    if(scan==NULL)
    {
        *mytsar=temp;
    }
    else
    {
        while(scan->next!=NULL)
            scan=scan->next;
        scan->next=temp;
    }
}
void destroytsar(tsar die,tsar **mytsar)
{

    tsar *target,*father;
    if((*mytsar)->x==die.x&&(*mytsar)->y==die.y)
    {
        target=*mytsar;
        *mytsar=target->next;
        free(target);
    }
    else
    {
        father=found(die,*mytsar);
        target=father->next;
        father->next=target->next;
        free(target);

    }



}
void explosionwave(tsar **mytsar,jet **myjet)
{

    tsar *scan=*mytsar;
    tsar *prev=scan;
    while(scan!=NULL)
    {

        if(scan->rad<60)
        {

            scan->rad+=0.5;

            jet *scanjet=*myjet;
            while(scanjet!=NULL)
            {
                double z=distan(truecenter(*scanjet),*scan);
                if(scan->rad+5>z)
                {
                    //printf("!!!!!!!!!!!!!!!!!!!!!!!");
                    creattsarbomb1(*scanjet,mytsar);
                    destroyjet(*scanjet,myjet);
                }
                scanjet=scanjet->next;
            }
            if(bossappear==1&&myboss.y>20)
            {

                if(myboss.health>=0)
                {
                    myboss.health-=25;
                }



            }



        }
        else
        {
            destroytsar(*scan,mytsar);
        }
        scan=scan->next;
    }
}
void printwave(tsar **mytsar)
{
    tsar *scan=*mytsar;
    while(scan!=NULL)
    {
        //printf("!!!!!!!!!!!!!!!!!!!");
        al_draw_circle(scan->x, scan->y,  scan->rad, al_map_rgb(100, 0, 255),5);

        scan=scan->next;
    }
}
