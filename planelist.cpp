#include <stdio.h>
#include <stdlib.h>
#include "planelist.h"
#include <math.h>
#include <assert.h>
#include "bullet.h"
#include "wavebomb.h"
//#include "tsarbomb.h"
static double distance(jet jetaxe,bullet bulletaxe)
{
    return sqrt((jetaxe.x-bulletaxe.x)*(jetaxe.x-bulletaxe.x)+(jetaxe.y-bulletaxe.y)*(jetaxe.y-bulletaxe.y));
}
static jet truecenter(jet jetaxe)
{
    if(jetaxe.isenemy==1)
    {
        jetaxe.x+=10;
        jetaxe.y+=20;

    }
    else if(jetaxe.isenemy==0)
    {
        jetaxe.x+=20;
        jetaxe.y+=15;

    }
    return jetaxe;
}
static void copynew(jet newjet,jet *node)
{
    *node=newjet;
}
static jet *found(jet target,jet *myjet)
{
    jet *scan,*temp;
    scan=myjet;
    temp=scan;
    int a=0;
    while(1)
    {
        if(scan->x==target.x&&scan->y==target.y)
        {

            return temp;
        }
        else
        {
            temp=scan;
            scan=scan->next;
        }
    }


}
void initializejet(jet **myjet)
{
    myjet=NULL;
}
bool jetisempty(const jet **myjet)
{
    if(myjet==NULL)
    {
        return true;
    }
    else
        return false;
}
bool jetisfull(const jet **myjet)
{
    jet *pt;
    pt=(jet*)malloc(sizeof(jet));
    int ans;
    if(pt==NULL)
    {
        free(pt);
        return true;
    }
    else
    {
        free(pt);
        return false;
    }
}
void addjet(jet newjet,jet **myjet)
{
    jet *temp;
    temp=(jet*)malloc(sizeof(jet));

    *temp=newjet;
    temp->next=NULL;

    jet *scan=*myjet;
    if(scan==NULL)
    {
        *myjet=temp;
    }
    else
    {
        while(scan->next!=NULL)
            scan=scan->next;
        scan->next=temp;
    }

}
void destroyjet(jet die,jet **myjet)
{

    if(die.isenemy!=0)
    {
        totalpoint+=1000;
        numyoukill+=1;
        jet *target,*father;
        if((*myjet)->x==die.x&&(*myjet)->y==die.y)
        {
            target=*myjet;
            *myjet=target->next;
            free(target);
        }
        else
        {
            father=found(die,*myjet);
            target=father->next;
            father->next=target->next;
            free(target);

        }
    }


}
void emptyjetlist(jet **myjet)
{
    jet *temp;
    while(*myjet!=NULL)
    {
        temp=(*myjet)->next;
        free(*myjet);
        *myjet=temp;
    }
}
void creatplane(int x,int y,int z,jet **myjet)
{
    jet newjet;
    newjet.x=x;
    newjet.y=y;
    newjet.mydir.way=z;
    newjet.isenemy=1;
    newjet.health=1000;
    newjet.image_path=enemyplane1;
    addjet(newjet,myjet);
}


void printplane(jet **myjet)
{
    jet *scanjet=*myjet;
    while(scanjet!=NULL)
    {

        if(scanjet->isenemy==0)
        {
            if(scanjet->playertype==1)
                al_draw_bitmap(scanjet->image_path, scanjet->x+7, scanjet->y+3, 0);
            else if(scanjet->playertype==2)
                al_draw_bitmap(player2, scanjet->x-18, scanjet->y, 0);
        }

        else
        {
            al_draw_bitmap(scanjet->image_path, scanjet->x, scanjet->y, 0);

        }
        scanjet=scanjet->next;

    }
}
void isplayerdie(jet **myjet,bullet **mybullet)
{

    jet *scanjet=*myjet;
    bullet *scanbullet=*mybullet;
    while(scanbullet!=NULL)
    {
        double dis;

        dis=distance(truecenter(*scanjet),*scanbullet);
        //  jet fuck=truecenter(*scanjet);
        //printf("(%lf %lf)    (%lf   %lf)   dis%lf   \n",fuck.x,fuck.y,scanbullet->x,scanbullet->y,dis);
        if(dis<20&&((scanbullet->isenemy>0&&scanjet->isenemy==0)||(scanbullet->isenemy==0&&scanjet->isenemy>0)))
        {
            if(scanbullet->isenemy==1)
            {
                scanjet->health-=100;
                if(scanjet->health<=0)
                {
                    //destroyjet(*scanjet, myjet);
                    playerdie=1;
                    break;
                }
            }
            else if(scanbullet->isenemy==2)
            {
                //printf("!!!!!!!!!!!!!!!!!!!!!!!!!1");
                scanjet->health-=500;
                if(scanjet->health<=0)
                {
                    //destroyjet(*scanjet, myjet);
                    playerdie=1;
                    break;
                }
            }

            destroybullet(*scanbullet,mybullet);
        }
        scanbullet=scanbullet->next;
    }
}
/*void isenemydie(jet **myjet,bullet **mybullet)
{
    jet *fuckjet=*myjet;
    bullet *fuckbullet=*mybullet;
    while(fuckjet!=NULL)
    {




        fuckjet=fuckjet->next;
    }
}*/
void isenemydie(jet **myjet,bullet **mybullet)
{
    jet *scanjet=*myjet;

    while(scanjet!=NULL)
    {
        bullet *scanbullet=*mybullet;
        while(scanbullet!=NULL)
        {
            double dis;
            dis=distance(truecenter(*scanjet),*scanbullet);

            if(dis<22)
            {
                if(scanbullet->bullettype==3)
                {
                    creattsarbomb(*scanbullet,&tsarhead);
                     destroyjet(*scanjet,myjet);
                    destroybullet(*scanbullet,mybullet);
                }
                else
                {

                    destroybullet(*scanbullet,mybullet);
                    scanjet->health-=100;
                    if(scanjet->health<=0)
                    {
                        destroyjet(*scanjet,myjet);

                    }
                }

            }
            scanbullet=scanbullet->next;
        }
        scanjet=scanjet->next;
    }
}
void caculateenemypos(jet **myjet)
{
    jet *scan=*myjet;

    while(scan!=NULL)
    {

        if(scan->mydir.way==1)
        {
            if(scan->x < 50) scan->mydir.dir = false;
            else if(scan->x > WIDTH-50) scan->mydir.dir = true;

            if(scan->mydir.dir) scan->x -= 2;
            else scan->x += 2;
        }
        scan=scan->next;
    }

}
