#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "varble.h"
#include "bullet.h"
#include "inter.h"
#include "planelist.h"
#include"boss.h"
#include "wavebomb.h"
//#include "tsarbomb.h"
#define GAME_TERMINATE -1

// ALLEGRO Variables
ALLEGRO_DISPLAY* display = NULL;
ALLEGRO_EVENT_QUEUE *event_queue = NULL;
ALLEGRO_BITMAP *playerimage = NULL;
ALLEGRO_BITMAP *playerbullet = NULL;
ALLEGRO_BITMAP *enemybullet1 = NULL;
ALLEGRO_BITMAP *background = NULL;
ALLEGRO_BITMAP *theboss = NULL;
ALLEGRO_BITMAP *bossbullet=NULL;
ALLEGRO_BITMAP *shopplayer= NULL;
ALLEGRO_BITMAP *player2= NULL;
ALLEGRO_BITMAP *playerbullet2= NULL;
ALLEGRO_BITMAP *shopplayer2=NULL;
ALLEGRO_BITMAP *wavebomb=NULL;
ALLEGRO_BITMAP *logo=NULL;
ALLEGRO_BITMAP *girl=NULL;
ALLEGRO_KEYBOARD_STATE keyState ;
ALLEGRO_TIMER *timer = NULL;
ALLEGRO_TIMER *timer2 = NULL;
ALLEGRO_TIMER *timer3 = NULL;
ALLEGRO_TIMER *timer4 = NULL;//producebullet
ALLEGRO_TIMER *timerjudgedie = NULL;
ALLEGRO_TIMER *timerproduceplane = NULL;
ALLEGRO_TIMER *timerbulletmove = NULL;
ALLEGRO_TIMER *timerplayerbullet = NULL;
ALLEGRO_TIMER *caculateplayerbullet = NULL;
ALLEGRO_TIMER *tsarspread = NULL;
ALLEGRO_TIMER *addpoint = NULL;
ALLEGRO_TIMER *bosslaser = NULL;
ALLEGRO_TIMER *wavebombcd = NULL;

ALLEGRO_FONT *forpoint=NULL;
ALLEGRO_FONT *forwinner=NULL;
ALLEGRO_SAMPLE *battle=NULL;
ALLEGRO_SAMPLE *exboss=NULL;
ALLEGRO_SAMPLE *song=NULL;
ALLEGRO_FONT *font = NULL;
ALLEGRO_FONT *forshop = NULL;
ALLEGRO_BITMAP *enemyplane1=NULL;
//Custom Definition
const char *title = "Final Project 107062304";
const float FPS = 60;
const int WIDTH = 400;
const int HEIGHT = 600;
int nowequip=1;
int isbuyplayer2=0;
int face=0;
int ifbomb=0;
int money=1200;
int preface=0;
int laserrand;
int playerdie=0;
int playertotalhealth=10000;
int totalpoint=0;
int imageWidth = 0;
int imageHeight = 0;
int draw = 0;
int done = 0;
int window = 1;
int numyoukill=0;
int bossappear=0;
int isbosslaser=0;
double laserwidth=0;
bool judge_next_window = false;
bool ture = true; //true: appear, false: disappear
bool next = false; //true: trigger
bool dir = true; //true: left, false: right
bullet *enemybullethead=NULL;
bullet *playerbullethead=NULL;
jet *jethead;
jet player;
tsar *tsarhead=NULL;
int enemybullettemp=0;
boss myboss;
void show_err_msg(int msg);
void game_init();
void game_begin();
int process_event();
int game_run();
void game_destroy();
void stopalltimer();
void startalltimer();
void printmenu();
void printshop();
void printabout();
//void printscore(int score);
//void printplayerhealth(int health);
int ifrad=0;
int ifcreatbomb=0;
double rad=0;
int enternextpage=0;
int bossswitch=0;
void initplayer()
{
    player.isenemy=0;
    player.x = WIDTH /2;
    player.y = HEIGHT / 2 + 150;
    player.next=NULL;
    player.playertype=1;
    player.health=playertotalhealth;
    jethead=&player;
}

int main(int argc, char *argv[])
{
    srand(time(NULL));
    int msg = 0;
    initplayer();
    game_init();
    game_begin();


    while (msg != GAME_TERMINATE)
    {
        /* if(playerdie==1)
         {
             printmenu();
         }*/
        msg = game_run();
        if(face==0)
        {
            printmenu();
        }
        if (msg == GAME_TERMINATE)
            printf("Game Over\n");
    }

    game_destroy();
    return 0;
}

void show_err_msg(int msg)
{
    fprintf(stderr, "unexpected msg: %d\n", msg);
    game_destroy();
    exit(9);
}

void game_init()
{
    if (!al_init())
    {
        show_err_msg(-1);
    }
    if(!al_install_audio())
    {
        fprintf(stderr, "failed to initialize audio!\n");
        show_err_msg(-2);
    }
    if(!al_init_acodec_addon())
    {
        fprintf(stderr, "failed to initialize audio codecs!\n");
        show_err_msg(-3);
    }
    if (!al_reserve_samples(1))
    {
        fprintf(stderr, "failed to reserve samples!\n");
        show_err_msg(-4);
    }
    // Create display
    display = al_create_display(WIDTH, HEIGHT);
    event_queue = al_create_event_queue();
    if (display == NULL || event_queue == NULL)
    {
        show_err_msg(-5);
    }
    // Initialize Allegro settings
    al_set_window_position(display, 0, 0);
    al_set_window_title(display, title);
    al_init_primitives_addon();
    al_install_keyboard();
    al_install_audio();
    al_init_image_addon();
    al_init_acodec_addon();
    al_init_font_addon();
    al_init_ttf_addon();
    al_install_mouse();
    al_register_event_source(event_queue, al_get_mouse_event_source());
    // Register event
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
}

void game_begin()
{
    // Load sound
    shopplayer=al_load_bitmap("shopplayer.png");
    shopplayer2=al_load_bitmap("shopplayer2.png");
    playerbullet2=al_load_bitmap("playerbullet2.png");
    song = al_load_sample( "hello.wav" );
    battle=al_load_sample( "battle.wav" );
    exboss=al_load_sample( "exboss.wav" );
    wavebomb=al_load_bitmap( "wavebomb.png" );
    girl=al_load_bitmap( "girl.png" );
    logo=al_load_bitmap( "logo.png" );
    if (!song)
    {
        printf( "Audio clip sample not loaded!\n" );
        //show_err_msg(-6);
    }
    // Loop the song until the display closes
    al_play_sample(song, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);

    // Load and draw text
    font = al_load_ttf_font("pirulen.ttf",12,0);
    forpoint=al_load_ttf_font("pirulen.ttf",20,0);
    forshop=al_load_ttf_font("pirulen.ttf",10,0);
    forwinner=al_load_ttf_font("pirulen.ttf",40,0);
    printmenu();


}

int process_event()
{
    // Request the event
    if(preface!=face||bossswitch!=bossappear)
    {
        preface=face;
        bossswitch=bossappear;
        al_stop_samples();
        if(face==0)
        {
            al_play_sample(song, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);
        }
        else if(face==1&&bossappear==1)
        {
            al_play_sample(exboss, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);

        }
        else if(face==1)
        {
            al_play_sample(battle, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);
        }

    }
    ALLEGRO_EVENT event;
    al_wait_for_event(event_queue, &event);
    if(playerdie==0)
    {


        if(face==1)
        {
            if(event.type == ALLEGRO_EVENT_MOUSE_AXES)
            {
                player.x = event.mouse.x-30;
                player.y = event.mouse.y-30;
            }
            // Our setting for controlling animation
            if(event.timer.source == tsarspread)
            {
                 explosionwave(&tsarhead,&(jethead->next));
            }
            if(event.timer.source == timer)
            {
                caculateenemypos(&(jethead->next));
                if(bossappear==1)
                {
                    if(myboss.way==0)
                    {
                        bossenter();
                        //printf("!!!!!!!!!!!!!!!!!!!!!!!");
                    }
                    if(myboss.way==1)
                    {
                        caculatebosspos1();
                    }

                }
            }

            if(event.timer.source == timer2)
            {
                ture = false;
                next = true;
            }
            if(event.timer.source == timer3)
            {
                if(next) next = false;
                else ture = true;
            }
            if(event.timer.source == timer4)
            {

                jet *scan;
                scan=jethead->next;
                while(scan!=NULL&&enemybullettemp!=3)
                {
                    //printf("!!!!!!!!!!!!!!!!!!!!");
                    createnemybullet(scan->x,scan->y,scan->isenemy,&enemybullethead);
                    scan=scan->next;
                }
                enemybullettemp+=1;
                if(enemybullettemp==4)
                    enemybullettemp=0;
                if(bossappear==1)
                {
                    creatbossbullet(&enemybullethead);
                }

            }
            if(event.timer.source == timerplayerbullet)
            {
                //if(jethead->playertype!=2)
                creatplayerbullet(&jethead,&playerbullethead);
            }
            if(event.timer.source == timerbulletmove)
            {
                caculateenemybulletpos(&enemybullethead);

            }
            if(event.timer.source == timerproduceplane)
            {
                int x,y,z;
                x=rand()%350+25;
                y=rand()%150+25;
                z=1;
                creatplane(x,y,z,&jethead);

            }
            if(event.timer.source == timerjudgedie)
            {
                isplayerdie(&jethead,&enemybullethead);
                isenemydie(&(jethead->next),&playerbullethead);
                if(bossappear==1)
                    isbossdie(&playerbullethead);
                if(ifrad==1)
                {
                    aoyibullet(&enemybullethead,rad);
                }
            }
            if(event.timer.source == caculateplayerbullet)
            {
                caculateplayerbulletpos(&jethead,&playerbullethead);
            }
            if(event.timer.source == bosslaser&&bossappear==1)
            {
                al_stop_timer(bosslaser);
                isbosslaser=1;
                laserrand=rand()%3;
                // printf("%d\n",laserrand);
            }
            if(event.timer.source == addpoint)
            {
                totalpoint+=11;
            }
            if(event.timer.source ==wavebombcd)
            {
                ifbomb=0;
                al_stop_timer(wavebombcd);
            }
            /*if(event.type == ALLEGRO_EVENT_MOUSE_AXES){
                player.x = event.mouse.x;
                player.y = event.mouse.y;
            }*/
            // Keyboard

            if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
            {
                if(event.mouse.button == 1&&jethead->playertype==1)
                    ifrad=1;
                if(event.mouse.button == 1&&jethead->playertype==2&&ifbomb==0)
                  {

                      al_start_timer(wavebombcd);
                      ifbomb=1;
                      ifcreatbomb=1;
                      creatplayerbullet(&jethead,&playerbullethead);
                  }

            }
        }
        if(face==0)
        {
            if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
            {
                if(event.mouse.button == 1)
                {
                    if(within(event.mouse.x,event.mouse.y,WIDTH/2-150, 510, WIDTH/2+150, 550))
                    {
                        face=1;
                        if(nowequip==1)
                            jethead->playertype=1;
                        else if(nowequip==2)
                            jethead->playertype=2;
                    }
                    if(within(event.mouse.x,event.mouse.y,WIDTH/2-150, 460, WIDTH/2+150, 500))
                    {
                        face=2;

                    }
                    if(within(event.mouse.x,event.mouse.y,WIDTH/2-150, 410, WIDTH/2+150, 450))
                    {
                        face=3;
                    }
                    if(within(event.mouse.x,event.mouse.y,WIDTH/2-150, 360, WIDTH/2+150, 400))
                    {
                        return GAME_TERMINATE;
                    }
                }

            }
        }
        if(face==3)
        {
            if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
            {
                if(event.mouse.button == 1)
                {
                    if(within(event.mouse.x,event.mouse.y,20, 470, 180, 510))
                    {
                        face=0;
                    }
                }
            }
        }
        if(face==2)
        {

            if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
            {
                if(event.mouse.button == 1)
                {
                    //printf("!!!!!!!!!!!!!!!!!!!!!!!");

                    if(within(event.mouse.x,event.mouse.y,30, 320, 170,360)&&money>=1000&&isbuyplayer2==0)
                    {

                        isbuyplayer2=1;
                        money-=1000;

                    }
                    if(isbuyplayer2==1&&within(event.mouse.x,event.mouse.y,30, 320, 170,360))
                    {
                        jethead->playertype=2;
                        nowequip=2;
                    }
                    if(within(event.mouse.x,event.mouse.y,220, 320, 360,360))
                    {
                        jethead->playertype=1;
                        nowequip=1;
                    }
                    if(within(event.mouse.x,event.mouse.y,WIDTH/2-150, 510, WIDTH/2+150, 550))
                    {
                        face=0;
                    }

                }


            }
        }

        // Shutdown our program
        else if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
            return GAME_TERMINATE;

    }
    if(jethead->health<=0)
    {
        playerdie=1;
    }
    if(playerdie==1)
    {

        stopalltimer();
        if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
        {
            if(event.mouse.button == 1)
            {

                if(myboss.health<=0&&within(event.mouse.x,event.mouse.y,WIDTH/2-150, 390, WIDTH/2+150, 430))
                {
                   money+=totalpoint/10;
                    face=0;
                    playerdie = 0;
                    emptyjetlist(&jethead);
                    emptybulletlist(&enemybullethead);
                    emptybulletlist(&playerbullethead);
                    jethead=NULL;
                    enemybullethead=NULL;
                    playerbullethead=NULL;
                    initplayer();
                    numyoukill=0;
                    bossappear=0;
                    isbosslaser=0;
                    laserwidth=0;
                    bossswitch=0;
                    totalpoint=0;
                }
                else if(within(event.mouse.x,event.mouse.y,WIDTH/2-150, 290, WIDTH/2+150, 330))
                {
                    money+=totalpoint/10;
                    face=0;
                    playerdie = 0;
                    emptyjetlist(&jethead);
                    emptybulletlist(&enemybullethead);
                    emptybulletlist(&playerbullethead);
                    jethead=NULL;
                    enemybullethead=NULL;
                    playerbullethead=NULL;
                    initplayer();
                    numyoukill=0;
                    bossappear=0;
                    isbosslaser=0;
                    laserwidth=0;
                    bossswitch=0;
                    totalpoint=0;
                }
            }
        }
        //  return -1;
    }
    return 0;
    if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
        return GAME_TERMINATE;

}

int game_run()
{
    int error = 0;
    // First window(Menu)
    if(face == 0)
    {
        if (!al_is_event_queue_empty(event_queue))
        {
            printmenu();
            error = process_event();


            if(face==1)
            {
                // Setting Character
                // printf("!!!!!!!!!!!!!!!!!!!!!!!!");

                player.image_path = al_load_bitmap("plane.png");

                background = al_load_bitmap("stage.jpg");
                playerbullet=al_load_bitmap("playerbullet.png");
                enemyplane1=al_load_bitmap("enemyplane1.png");
                enemybullet1=al_load_bitmap("enemybullet1.png");
                theboss=al_load_bitmap("boss.png");
                bossbullet=al_load_bitmap("bossbullet.png");
                player2=al_load_bitmap("player2.png");

                //Initialize Timer
                timer  = al_create_timer(1.0/30.0);
                timer2  = al_create_timer(1.0);
                timer3  = al_create_timer(1.0/10.0);
                timer4=al_create_timer(1.0);
                timerproduceplane=al_create_timer(1.0/2.0);
                timerjudgedie=al_create_timer(1.0/100.0);
                timerbulletmove=al_create_timer(1.0/30.0);
                timerplayerbullet=al_create_timer(1.0/10.0);
                caculateplayerbullet=al_create_timer(1.0/30.0);
                addpoint=al_create_timer(1.0/10.0);
                bosslaser=al_create_timer(5.0);
                wavebombcd=al_create_timer(1.0);
                tsarspread=al_create_timer(1.0/120.0);

                al_register_event_source(event_queue, al_get_timer_event_source(timer)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timer2)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timer3)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timer4)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timerproduceplane)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timerjudgedie)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timerbulletmove)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(timerplayerbullet)) ;
                al_register_event_source(event_queue, al_get_timer_event_source(caculateplayerbullet));
                al_register_event_source(event_queue, al_get_timer_event_source(addpoint));
                al_register_event_source(event_queue, al_get_timer_event_source(bosslaser));
                al_register_event_source(event_queue, al_get_timer_event_source(wavebombcd));
                al_register_event_source(event_queue, al_get_timer_event_source(tsarspread));
                startalltimer();
            }


        }
    }
    // Second window(Main Game)
    else if(face == 1)
    {
        // Change Image for animation
        if(numyoukill>0&&bossappear==0)
        {
            createboss();
            //printf("!!!!!!!!!!!!!!!!!!!!!!");
            bossappear=1;

            al_start_timer(bosslaser);
        }

        al_draw_bitmap(background, 0,0, 0);
        //if(ture) al_draw_bitmap(player.image_path, player.x, player.y, 0);
        //al_draw_bitmap(theboss, 95,-150 , 0);


        printbullet(&enemybullethead);
        printbullet(&playerbullethead);
        if(isbosslaser==1)
        {
            if(laserrand==0)
            {
                if(laserwidth<30)
                {
                    laserwidth+=0.05;
                    printleftlaser(laserwidth);
                }
                else if(laserwidth<31)
                {
                    laserwidth+=0.0005;
                    printleftlaser(laserwidth);
                }
                else if(laserwidth<60)
                {
                    laserwidth+=0.05;
                    printleftlaser(60-laserwidth);
                }
                else
                {
                    laserwidth=0;
                    isbosslaser=0;
                    al_start_timer(bosslaser);
                }
            }
            else if(laserrand==1)
            {
                if(laserwidth<30)
                {
                    laserwidth+=0.05;
                    printrightlaser(laserwidth);
                }
                else if(laserwidth<31)
                {
                    laserwidth+=0.0005;
                    printrightlaser(laserwidth);
                }
                else if(laserwidth<60)
                {
                    laserwidth+=0.05;
                    printrightlaser(60-laserwidth);
                }
                else
                {
                    laserwidth=0;
                    isbosslaser=0;
                    al_start_timer(bosslaser);
                }
            }
            else
            {
                if(laserwidth<30)
                {
                    laserwidth+=0.05;
                    printleftlaser(laserwidth);
                    printrightlaser(laserwidth);
                }
                else if(laserwidth<31)
                {
                    laserwidth+=0.0005;
                    printleftlaser(laserwidth);
                    printrightlaser(laserwidth);
                }
                else if(laserwidth<60)
                {
                    laserwidth+=0.05;
                    printleftlaser(60-laserwidth);
                    printrightlaser(60-laserwidth);
                }
                else
                {
                    laserwidth=0;
                    isbosslaser=0;
                    al_start_timer(bosslaser);
                }
            }

        }
        printplane(&jethead);

        if(bossappear==1)
        {
            //printf("!!!!!!!!!!!!!!!!!!!!!!");
            printboss();

        }

        if(ifrad>0)
        {
            aoyi(jethead,rad);
            rad+=0.35;
            if(rad>3000)
            {
                rad=0;
                ifrad=0;
            }
        }

           printwave(&tsarhead);


        printplayerhealth(jethead->health);
        printscore(totalpoint);
        if(playerdie==1)
        {
            restart();

        }

        al_flip_display();
        al_clear_to_color(al_map_rgb(0,0,0));

        // Listening for new event
        if (!al_is_event_queue_empty(event_queue))
        {
            error = process_event();
        }
    }
    else if (face == 2)
    {
        if (!al_is_event_queue_empty(event_queue))
        {
            error = process_event();
        }
        printshop();
    }
    else if(face == 3)
    {
        if (!al_is_event_queue_empty(event_queue))
        {
            error = process_event();
        }
        printabout();
    }


    return error;
}

void game_destroy()
{
    // Make sure you destroy all things
    al_destroy_event_queue(event_queue);
    al_destroy_display(display);
    al_destroy_timer(timer);
    al_destroy_timer(timer2);

    al_destroy_sample(song);
}
void stopalltimer()
{
    al_stop_timer(timer);
    al_stop_timer(timer2);
    al_stop_timer(timer3);
    al_stop_timer(timer4);
    al_stop_timer(timerproduceplane);
    al_stop_timer(timerjudgedie);
    al_stop_timer(timerbulletmove);
    al_stop_timer(timerplayerbullet);
    al_stop_timer(caculateplayerbullet);
    al_stop_timer(addpoint);
    al_stop_timer(bosslaser);
    al_stop_timer(wavebombcd);
}
void startalltimer()
{
    al_start_timer(timer);
    al_start_timer(timer2);
    al_start_timer(timer3);
    al_start_timer(timer4);
    al_start_timer(timerproduceplane);
    al_start_timer(timerjudgedie);
    al_start_timer(timerbulletmove);
    al_start_timer(timerplayerbullet);
    al_start_timer(caculateplayerbullet);
    al_start_timer(addpoint);
    al_start_timer(tsarspread);
}
void printmenu()
{
    //al_play_sample(song, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);
    al_clear_to_color(al_map_rgb(100,100,100));
    char moneys[50];
    sprintf(moneys,"%d",money);
    al_draw_text(font, al_map_rgb(255,255,255), 80,30, ALLEGRO_ALIGN_CENTRE, "YOUR MONEY");
    al_draw_text(forpoint, al_map_rgb(255,255,255), 80, 50, ALLEGRO_ALIGN_CENTRE,moneys );
    al_draw_bitmap(logo,  40,110, 0);
    // Load and draw text
    al_draw_text(font, al_map_rgb(255,255,255), WIDTH/2, HEIGHT/2+220, ALLEGRO_ALIGN_CENTRE, "START");
    al_draw_rectangle(WIDTH/2-150, 510, WIDTH/2+150, 550, al_map_rgb(255, 255, 255), 0);
    al_draw_text(font, al_map_rgb(255,255,255), WIDTH/2, HEIGHT/2+170, ALLEGRO_ALIGN_CENTRE, "SHOP");
    al_draw_rectangle(WIDTH/2-150, 460, WIDTH/2+150, 500, al_map_rgb(255, 255, 255), 0);
    al_draw_text(font, al_map_rgb(255,255,255), WIDTH/2, HEIGHT/2+120, ALLEGRO_ALIGN_CENTRE, "ABOUT");
    al_draw_rectangle(WIDTH/2-150, 410, WIDTH/2+150, 450, al_map_rgb(255, 255, 255), 0);
    al_draw_text(font, al_map_rgb(255,255,255), WIDTH/2, HEIGHT/2+70, ALLEGRO_ALIGN_CENTRE, "EXIT");
    al_draw_rectangle(WIDTH/2-150, 360, WIDTH/2+150, 400, al_map_rgb(255, 255, 255), 0);
    al_flip_display();
}
void printshop()
{
    al_clear_to_color(al_map_rgb(0,0,153));
    al_draw_bitmap(shopplayer2,  -20, 150, 0);
    al_draw_bitmap(shopplayer,  210, 150, 0);
    if(nowequip==1)
        al_draw_text(forshop, al_map_rgb(255,255,255), 290, HEIGHT/2+80, ALLEGRO_ALIGN_CENTRE, "NOW USING");
    else if(nowequip==2)
        al_draw_text(forshop, al_map_rgb(255,255,255), 100, HEIGHT/2+80, ALLEGRO_ALIGN_CENTRE, "NOW USING");
    if(isbuyplayer2==0)
        al_draw_text(forshop, al_map_rgb(255,255,255), 100, HEIGHT/2+30, ALLEGRO_ALIGN_CENTRE, "1000 DOLLARS");
    else if(isbuyplayer2==1)
        al_draw_text(forshop, al_map_rgb(255,255,255), 100, HEIGHT/2+30, ALLEGRO_ALIGN_CENTRE, "USE IT!");
    al_draw_rectangle(30, 320, 170,360, al_map_rgb(255, 255, 255), 0);
    al_draw_rectangle(220, 320, 360,360, al_map_rgb(255, 255, 255), 0);
    al_draw_text(forshop, al_map_rgb(255,255,255), 290, HEIGHT/2+30, ALLEGRO_ALIGN_CENTRE, "USE IT!");
    al_draw_text(font, al_map_rgb(255,255,255), WIDTH/2, HEIGHT/2+220, ALLEGRO_ALIGN_CENTRE, "BACK TO MENU");
    al_draw_rectangle(WIDTH/2-150, 510, WIDTH/2+150, 550,al_map_rgb(255, 255, 255), 0);

    al_flip_display();
}
void printabout()
{
    al_clear_to_color(al_map_rgb(0,153,255));
    al_draw_text(font, al_map_rgb(255,255,255), 200,50, ALLEGRO_ALIGN_CENTRE, "YOU ARE HERO");
    al_draw_text(font, al_map_rgb(255,255,255), 200,100, ALLEGRO_ALIGN_CENTRE, "YOUR DESTINY IS TO DEFEAT BOSS");
    al_draw_text(font, al_map_rgb(255,255,255), 200,150, ALLEGRO_ALIGN_CENTRE, "TRY TO USE YOUR MOUSE TO");
    al_draw_text(font, al_map_rgb(255,255,255), 200,200, ALLEGRO_ALIGN_CENTRE, "DODGE FROM BULLET!!!!!!!!!!");
    al_draw_text(font, al_map_rgb(255,255,255), 100,270, ALLEGRO_ALIGN_CENTRE, "PLEASE");
    al_draw_text(font, al_map_rgb(255,255,255), 100,300, ALLEGRO_ALIGN_CENTRE, "GIVE");
    al_draw_text(font, al_map_rgb(255,255,255), 100,330, ALLEGRO_ALIGN_CENTRE, "ME");
    al_draw_text(font, al_map_rgb(255,255,255), 100,360, ALLEGRO_ALIGN_CENTRE, "HIGH");
    al_draw_text(font, al_map_rgb(255,255,255), 100,390, ALLEGRO_ALIGN_CENTRE, "POINTS");
    al_draw_text(font, al_map_rgb(255,255,255), 100,420, ALLEGRO_ALIGN_CENTRE, "!!!!!!!!!!");
    al_draw_text(font, al_map_rgb(255,255,255), 100,480, ALLEGRO_ALIGN_CENTRE, "BACK TO MENU");
    al_draw_rectangle(20, 470, 180, 510,al_map_rgb(255, 255, 255), 0);
    al_draw_bitmap(girl,  150, 250, 0);
    al_flip_display();
}
