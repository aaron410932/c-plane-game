#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "bullet.h"
static double distance(jet jetaxe,bullet bulletaxe)
{
    return sqrt((jetaxe.x-bulletaxe.x)*(jetaxe.x-bulletaxe.x)+(jetaxe.y-bulletaxe.y)*(jetaxe.y-bulletaxe.y));
}
static void copynew(bullet newbullet,bullet *node)
{
    *node=newbullet;
}
static bullet *found(bullet target,bullet *mybullet)
{
    bullet *scan,*temp;
    scan=mybullet;
    while(1)
    {
        if(scan->x==target.x&&scan->y==target.y)
        {
            return temp;
        }
        else
        {
            temp=scan;
            scan=scan->next;

        }
    }


}
void initializebullet(bullet **mybullet)
{
    *mybullet=NULL;
}
bool bulletisempty(const bullet **mybullet)
{
    if(*mybullet==NULL)
    {
        return true;
    }
    else
        return false;
}
bool bulletisfull(const bullet **mybullet)
{
    bullet *pt;
    pt=(bullet*)malloc(sizeof(bullet));
    int ans;
    if(pt==NULL)
    {
        free(pt);
        return true;
    }
    else
    {
        free(pt);
        return false;
    }
}
void addbullet(bullet newbullet,bullet **mybullet)
{
    bullet *temp;
    temp=(bullet*)malloc(sizeof(bullet));

    *temp=newbullet;
    temp->next=NULL;

    bullet *scan=*mybullet;
    if(scan==NULL)
    {
        *mybullet=temp;
        // printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }
    else
    {
        //printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        while(scan->next!=NULL)
            scan=scan->next;
        scan->next=temp;
    }

}
void destroybullet(bullet die,bullet **mybullet)
{

    bullet *target,*father;
    if((*mybullet)->x==die.x&&(*mybullet)->y==die.y)
    {
        target=*mybullet;
        *mybullet=target->next;
       // printf("destroy %d\n", target->bullettype);
        free(target);
    }
    else
    {
        father=found(die,*mybullet);
        target=father->next;
        father->next=target->next;
       // printf("destroy %d\n", target->bullettype);
        free(target);
    }

}
void aoyibullet(bullet **mybullet,double dis)
{
    bullet *scan=*mybullet;
    while(scan!=NULL)
    {
        double z=distance(*jethead,*scan);
        if(dis+10>z&&dis-10<z)
        {
            //printf("%lf  %lf\n",dis,z);
            destroybullet(*scan,mybullet);
        }
        scan=scan->next;
    }
}
void emptybulletlist(bullet **mybullet)
{
    bullet *temp;
    while(*mybullet!=NULL)
    {
        temp=(*mybullet)->next;
        free(*mybullet);
        *mybullet=temp;
    }
}
void createnemybullet(int a,int b,int isen,bullet **mybullet)
{
    bullet newbullet;

    if(isen==1)
    {
        newbullet.x=a+18;
        newbullet.y=b+20;
        newbullet.way=0;//down
        newbullet.isenemy=1;
        newbullet.next=NULL;
    }
    addbullet(newbullet,mybullet);



}
void printbullet(bullet **mybullet)
{
    bullet *scanbullet=*mybullet;

    while(scanbullet!=NULL)
    {
        //printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        if(scanbullet->isenemy==0)
        {
            if(scanbullet->bullettype==1)
                al_draw_bitmap(playerbullet, scanbullet->x+8, scanbullet->y+3, 0);
            else if(scanbullet->bullettype==2)
                al_draw_bitmap(playerbullet2, scanbullet->x-7, scanbullet->y+10, 0);
            else if(scanbullet->bullettype==3)
                al_draw_bitmap(wavebomb, scanbullet->x-7, scanbullet->y+10, 0);
        }

        else if(scanbullet->isenemy==1)
        {
            al_draw_bitmap(enemybullet1, scanbullet->x-5, scanbullet->y, 0);
        }
        else if(scanbullet->isenemy==2)
        {
            al_draw_bitmap(bossbullet, scanbullet->x-5, scanbullet->y, 0);
        }
        scanbullet=scanbullet->next;
    }

}
void caculateenemybulletpos(bullet **mybullet)
{
    bullet *scanbullet=*mybullet;

    while(scanbullet!=NULL&&scanbullet->isenemy!=0)
    {

        if(scanbullet->x<-20||scanbullet->x>420||scanbullet->y<-10||scanbullet->y>620)
        {
            destroybullet(*scanbullet,mybullet);
        }

        else
        {
            if(scanbullet->way==1)
            {

                scanbullet->y=scanbullet->y-2;
            }
            else if(scanbullet->way==0)
            {
                if(scanbullet->isenemy==1)
                    scanbullet->y=scanbullet->y+2;
                else if(scanbullet->isenemy==2)
                    scanbullet->y=scanbullet->y+3;
            }
            else if(scanbullet->way==2)
            {
                scanbullet->x+=1;
                scanbullet->y+=2;
            }
            else if(scanbullet->way==3)
            {
                scanbullet->x-=1;
                scanbullet->y+=2;
            }

        }
        scanbullet=scanbullet->next;

        //}
    }
}
void creatplayerbullet(jet **myjet,bullet **mybullet)
{
    bullet newbullet;



    newbullet.x=(*myjet)->x+18;
    newbullet.y=(*myjet)->y-1;
    newbullet.way=1;//up
    newbullet.isenemy=0;
    if(ifcreatbomb==0)
    newbullet.bullettype=(*myjet)->playertype;
    else{
         newbullet.bullettype=3;
         ifcreatbomb--;
    }

    addbullet(newbullet,mybullet);
}
void caculateplayerbulletpos(jet **myjet,bullet **mybullet)
{
    bullet *scanbullet=*mybullet;


    while(scanbullet!=NULL&&scanbullet->isenemy==0)
    {

        if(scanbullet->x<-20||scanbullet->x>420||scanbullet->y<-20||scanbullet->y>620)
        {
           destroybullet(*scanbullet,mybullet);


          /*  if(prev==*mybullet)
            {
                *mybullet=(*mybullet)->next;
                free(prev);
            }
            else
            {
                prev->next=scanbullet->next;
                free(scanbullet);
            }*/

        }
        else
        {
            if(scanbullet->bullettype==3)
            {
                scanbullet->y=scanbullet->y-5;
            }
            else if(scanbullet->way==1)
            {

                scanbullet->y=scanbullet->y-30;
            }
            else if(scanbullet->way==0)
            {

                scanbullet->y=scanbullet->y+30;
            }

        }
        //prev=scanbullet;
        scanbullet=scanbullet->next;

    }
}
void creatbossbullet(bullet **mybullet)
{
    bullet newbullet;
    newbullet.x=myboss.x+100;
    newbullet.y=myboss.y+70;
    newbullet.way=0;//up
    newbullet.isenemy=2;
    addbullet(newbullet,mybullet);
    newbullet.x=myboss.x+100;
    newbullet.y=myboss.y+70;
    newbullet.way=2;//up
    newbullet.isenemy=2;
    addbullet(newbullet,mybullet);
    newbullet.x=myboss.x+100;
    newbullet.y=myboss.y+70;
    newbullet.way=3;//up
    newbullet.isenemy=2;
    addbullet(newbullet,mybullet);
}
